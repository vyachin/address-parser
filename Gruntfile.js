const sass = require("node-sass");
const tilde_importer = require("grunt-sass-tilde-importer");
const babel = require("@rollup/plugin-babel");
const resolve = require("@rollup/plugin-node-resolve");
const commonjs = require("@rollup/plugin-commonjs");
const legacy = require("@rollup/plugin-legacy");

module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON("package.json"),
        copy: {
            default: {
                files: [
                    {
                        expand: true,
                        cwd: "address_parser/assets/img/",
                        src: "**",
                        dest: "address_parser/static/img/"
                    },
                    {
                        src: "address_parser/assets/favicon.ico",
                        dest: "address_parser/static/favicon.ico"
                    },
                    {
                        expand: true,
                        cwd: "./node_modules/@fortawesome/fontawesome-free/webfonts/",
                        src: "**",
                        dest: "address_parser/static/fonts/"
                    },
                ]
            }
        },
        sass: {
            default: {
                options: {
                    implementation: sass,
                    sourceMap: false,
                    importer: tilde_importer
                },
                files: {
                    "address_parser/static/css/style.css": "address_parser/assets/scss/style.scss",
                }
            }
        },
        rollup: {
            options: {
                format: "iife",
                plugins: [
                    resolve(),
                    babel.default({
                        babelHelpers: 'bundled',
                        babelrc: false,
                        presets: [
                            [
                                "@babel/preset-env",
                                {
                                    "targets": {
                                        "browsers": [
                                            "last 1 version",
                                            "> 1%",
                                            "IE 6"
                                        ]
                                    },
                                    "loose": true,
                                    "modules": false,
                                    "useBuiltIns": false,
                                }
                            ],
                        ]
                    }),
                    legacy({
                        'node_modules/highlight.js/lib/core.js': 'highlight',
                        'node_modules/highlight.js/lib/languages/json.js': 'json',
                    }),
                    commonjs({
                        include: ['node_modules/highlight.js/**', 'node_modules/bootstrap.native/**']
                    }),
                ],
            },
            default: {
                options: {
                    name: "AP"
                },
                files: {
                   "address_parser/static/js/script.js": "address_parser/assets/js/entry.js"
                }
            }
        },
        watch: {
            options: { livereload: false, spawn: false },
            styles: {
                files: ["address_parser/assets/**/*.scss"],
                tasks: ["sass"],
            },
            scripts: {
                files: ["address_parser/assets/**/*.js"],
                tasks: ["rollup"],
            },
            images: {
                files: ["address_parser/assets/**/*.{png,jpg}"],
                tasks: ["copy"],
            },
        },
    });

    grunt.loadNpmTasks("grunt-sass");
    grunt.loadNpmTasks("grunt-rollup");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-contrib-copy");

    grunt.registerTask("default", ["sass", "rollup", "copy"]);
};
