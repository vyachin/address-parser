import os

import sentry_sdk
from flask import Flask, url_for, session
from flask_migrate import Migrate
from flask_redis import FlaskRedis
from flask_session import Session
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from sendgrid import SendGridAPIClient
from sentry_sdk.integrations.flask import FlaskIntegration

from .tagger import load_tagger

db = SQLAlchemy()
migrate = Migrate()
flask_redis = FlaskRedis()
flask_session = Session()


def create_app(config):
    app = Flask(__name__)
    app.config.from_object(config)

    db.init_app(app)
    migrate.init_app(app, db)
    flask_redis.init_app(app)
    flask_session.init_app(app)

    from .core import bp as core_bp
    app.register_blueprint(core_bp)

    from .api import bp as api_bp
    app.register_blueprint(api_bp)

    app.tagger = load_tagger(app.config['TAGGER_FILE'])
    app.sendGrid = SendGridAPIClient(app.config['SENDGRID_API_KEY'])

    if not app.debug and not app.testing:
        sentry_sdk.init(dsn=app.config['SENTRY_DSN'], integrations=[FlaskIntegration()])

    @app.context_processor
    def jinja_utils():
        def _file_url(filename):
            filepath = os.path.join(app.static_folder, filename)
            fileurl = url_for('static', filename=filename)
            filetime = int(os.path.getmtime(filepath))
            return "%s?_=%d" % (fileurl, filetime)

        def _is_authority():
            return 'user_id' in session

        def _logout_form():
            return FlaskForm()

        return dict(file_url=_file_url, is_authority=_is_authority, logout_form=_logout_form)

    return app


from . import models
