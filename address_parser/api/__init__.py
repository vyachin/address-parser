from flask import Blueprint, render_template
from flask_restplus import Api, Namespace

bp = Blueprint('api', __name__, url_prefix='/api')

api = Api(bp, version='1.0', title='address-parser.ru',
          authorizations={
              "apikey": {
                  "type": "apiKey",
                  "in": "header",
                  "name": "X-API-KEY"
              }
          },
          description='Сервис разбивает строку почтового адреса на компоненты - страна, регион, город, улица, номер дома')

ns = Namespace('parser', description='Разбор строки почтового адреса на составляющие')
api.add_namespace(ns)


@api.documentation
def custom_ui():
    return render_template('swagger.html', title=api.title, specs_url=api.specs_url)


from . import routes
