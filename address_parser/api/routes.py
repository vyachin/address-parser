from flask import current_app, request
from flask_restplus import fields, Resource, abort

from address_parser import db
from address_parser.api import ns, api
from address_parser.models import User, Request
from address_parser.parser import prepare_address, address_tokenize, address_parts_from_tags

REQUEST_COST = 0.5

requestModel = api.model('Request', {
    'address': fields.String(required=True, description='Строка почтового адреса',
                             example='127427, г. Москва, ул. Академика Королева, д. 15, к. 2')
})

addressModel = api.model('Address', {
    'country': fields.String(example='РОССИЯ'),
    'city': fields.String(example='МОСКВА'),
    'republic': fields.String(example=None),
    'street': fields.String(example='УЛ. АКАДЕМИКА КОРОЛЕВА'),
    'region': fields.String,
    'house': fields.String(example='15'),
    'flat': fields.String,
    'district': fields.String,
    'block': fields.String(example='2'),
    'office': fields.String,
    'level': fields.String,
    'room': fields.String,
    'postcode': fields.String(example='127427'),
    'village': fields.String,
    'mail_box': fields.String,
})

resultModel = api.model('Result', {
    'success': fields.Boolean,
    'balance': fields.Float(example=123.45),
    'result': fields.Nested(addressModel, skip_none=True)
})


@ns.route('/')
@ns.doc(security='apikey')
class AddressParser(Resource):
    @api.marshal_with(resultModel, skip_none=True)
    @ns.expect(requestModel, validate=True)
    @api.response(403, 'Not Authorized')
    def post(self):
        api_key = request.headers.get('x-api-key')
        if api_key is None:
            abort(400, 'X-API-KEY header not found')

        user = User.query.filter_by(api_key=api_key).first()
        if user is None:
            abort(403, 'Not Authorized')
        if user.balance < REQUEST_COST:
            abort(403, 'Insufficient funds')

        user.balance -= REQUEST_COST
        db.session.add(user)
        address = str(api.payload['address'])
        clean_address = prepare_address(address)
        tokens = address_tokenize(clean_address)
        tags = current_app.tagger.tag(tokens)
        data = address_parts_from_tags(tags)

        req = Request()
        req.user_id = user.id
        req.address = address
        req.result = ' '.join(["%s/%s" % i for i in tags])

        db.session.add(req)
        db.session.commit()
        return {'success': True, 'balance': user.balance, 'result': data}
