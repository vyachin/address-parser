import highlight from "highlight.js/lib/core.js";
import json_language from "highlight.js/lib/languages/json.js"
import {init} from '@sentry/browser';
import "bootstrap.native/dist/bootstrap-native-v4.js";

init({
  dsn: "https://9f739b0944f5432bbf5ae921dcdf7b66@sentry.io/2612859"
});

function onViewPort(selector, callback) {
    let observer = new IntersectionObserver(
        (entries, observer) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    observer.unobserve(entry.target);
                    callback(entry.target);
                }
            });
        },
        {
            threshold: 0.5
        }
    );
    const targets = document.querySelectorAll(selector);
    targets.forEach(i => observer.observe(i));
}

const animateItems = target => {
    if (target.classList.contains("visible"))
        return;
    const startAnimation = () => {
        target.classList.add(target.dataset.animation);
        target.classList.add("visible");
    };
    if ("animationDelay" in target.dataset) {
        setTimeout(startAnimation, target.dataset.animationDelay);
    } else {
        startAnimation();
    }
}

const counterItems = target => {
    const style = target.dataset.counterStyle || "decimal";
    const decimals = style === "percent" ? 2 : 0;
    const formatter = new Intl.NumberFormat('ru-RU', { style: style, minimumFractionDigits: decimals, maximumFractionDigits: decimals });
    const from = parseFloat(target.dataset.counterStart || 0);
    const to = parseFloat(target.dataset.counterEnd || 0);
    const speed = parseFloat(target.dataset.counterSpeed || 1000);
    const refreshInterval = parseInt(target.counterRefreshInterval || 100);
    const loops = Math.ceil(speed / refreshInterval),
        increment = (to - from) / loops;
    let i = from;
    let loopCount = 0;
    const interval = setInterval(function () {
        i += increment;
        loopCount++;
        target.innerHTML = formatter.format(i);
        if (loopCount >= loops) {
            clearInterval(interval);
        }
    }, refreshInterval);
}

document.addEventListener("DOMContentLoaded", () => {
    highlight.registerLanguage('json', json_language);
    highlight.initHighlighting();
    onViewPort(".animated", animateItems);
    onViewPort(".counter", counterItems);
});
