from flask import Blueprint

from .forms import LogoutForm

bp = Blueprint('core', __name__, static_folder='../static', template_folder='templates')
SESSION_USER_ID = 'user_id'

from . import routes
