import datetime
import hashlib
import json
import secrets

import sentry_sdk
from flask import render_template, current_app, url_for, session, request, flash, Response
from jinja2 import Environment
from sendgrid.helpers.mail import Mail
from werkzeug.exceptions import abort
from werkzeug.utils import redirect

from . import bp, SESSION_USER_ID
from .forms import DemoForm, RegisterForm, LoginForm
from .. import db, flask_redis
from ..models import User, Request
from ..parser import prepare_address, address_tokenize, address_parts_from_tags

START_BALANCE = 10
REQUEST_COST = 0.5
REDIS_PREFIX_REGISTRATION_CONFIRM = 'registration-confirm'


def send_email(template_name, email, subject, data):
    env = Environment(loader=current_app.jinja_loader)
    template = env.get_template(template_name)
    html_content = template.render(data)

    message = Mail(
        from_email=current_app.config['FROM_EMAIL'],
        to_emails=email,
        subject=subject,
        html_content=html_content)
    try:
        response = current_app.sendGrid.send(message)
        current_app.logger.info(response)
    except Exception as e:
        sentry_sdk.capture_exception(e)


class ParseException(Exception):
    def __init__(self, msg):
        self.msg = msg


@bp.route('/')
def index():
    return render_template('index.html')


@bp.app_errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404


@bp.app_errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html', error=error), 500


@bp.route('/favicon.ico')
def favicon():
    return bp.send_static_file('favicon.ico')


@bp.route('/demo', methods=('GET', 'POST'))
def demo():
    form = DemoForm()
    if form.validate_on_submit():

        address = str(form.address.data)
        clean_address = prepare_address(address)
        tokens = address_tokenize(clean_address)
        tags = current_app.tagger.tag(tokens)
        data = address_parts_from_tags(tags)

        req = Request()
        req.address = address
        req.result = ' '.join(["%s/%s" % i for i in tags])

        db.session.add(req)
        db.session.commit()
        result = json.dumps({'success': True, 'data': data},
                            ensure_ascii=False, indent=4)
    else:
        result = None
    return render_template('demo.html', result=result, form=form)


@bp.route('/register', methods=('GET', 'POST'))
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        user = User(email=form.email.data)
        user.set_password(form.password.data)
        user.generate_api_key()
        user.balance = START_BALANCE
        user.register_at = datetime.datetime.utcnow()
        user.login_at = datetime.datetime.utcnow()
        db.session.add(user)
        db.session.commit()

        hash = secrets.token_urlsafe(64)
        flask_redis.set('%s:%s' % (
            REDIS_PREFIX_REGISTRATION_CONFIRM, hash), user.id, ex=86400 * 2)
        url = url_for('core.confirm', hash=hash, _external=True)
        send_email(
            'mail/confirm_registration.html',
            user.email,
            'Добро пожаловать на AddressParser! Подтвердите регистрацию',
            {'url': url, 'email': user.email}
        )
        flash(
            'Вы успешно зарегистрировались. На вашу электронную почту отправлено письмо для подтверждения вашего адреса.')
        return redirect(url_for('core.register'))
    return render_template('register.html', form=form)


@bp.route('/confirm/<hash>')
def confirm(hash):
    user_id = flask_redis.get('%s:%s' % (
        REDIS_PREFIX_REGISTRATION_CONFIRM, hash))
    if user_id is None:
        abort(404)
    user = User.query.get(int(user_id))
    if user is None or user.status != User.STATUS_NEW:
        abort(404)
    user.status = User.STATUS_CONFIRM
    user.login_at = datetime.datetime.utcnow()
    db.session.add(user)
    db.session.commit()
    session[SESSION_USER_ID] = user.id
    flash('Ваш адрес электронной почты подтвержден.')
    return redirect(url_for('core.profile'))


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if SESSION_USER_ID in session:
        next = request.args.get('next')
        return redirect(next or url_for('core.profile'))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(
            email=form.email.data, status=User.STATUS_CONFIRM).first()
        if user is None or not user.check_password(form.password.data):
            form.password.errors.append('Неправильный email или пароль')
            return render_template('login.html', form=form)

        user.login_at = datetime.datetime.utcnow()
        db.session.add(user)
        db.session.commit()
        session[SESSION_USER_ID] = user.id
        next = request.args.get('next')
        return redirect(next or url_for('core.profile'))
    return render_template('login.html', form=form)


@bp.route('/logout', methods=['POST'])
def logout():
    if SESSION_USER_ID in session:
        session.pop(SESSION_USER_ID)

    return redirect(url_for('core.index'))


@bp.route('/profile')
def profile():
    if SESSION_USER_ID not in session:
        abort(403)
    user = User.query.get(session[SESSION_USER_ID])

    prices = [{'price': i, 'requests': int(
        i / REQUEST_COST)} for i in [10, 50, 100]]
    return render_template('profile.html', user=user, prices=prices, receiver=current_app.config['RECEIVER'])


def check_yandex_hash(form):
    fields = [
        form['notification_type'],
        form['operation_id'],
        form['amount'],
        form['currency'],
        form['datetime'],
        form['sender'],
        form['codepro'],
        current_app.config['YANDEX_SECRET'],
        form['label']
    ]

    fields_str = '&'.join(fields)
    h = hashlib.sha1()
    h.update(fields_str.encode())
    return form['sha1_hash'] == h.hexdigest()


@bp.route('/yandex-money-notify', methods=['POST'])
def yandex_money_notify():
    form = request.form
    current_app.logger.info(form)

    if not check_yandex_hash(form):
        current_app.logger.error("Invalid hash")
        return ''

    if 'test_notification' in form:
        current_app.logger.info("Test notification")
        return ''

    if form['codepro'] == 'true':
        current_app.logger.info("Code protection")
        return ''

    rows = User.query.filter_by(email=form['label']).update(
        {User.balance: User.balance + form['withdraw_amount']})
    db.session.commit()
    if rows == 1:
        user = User.query.filter_by(email=form['label']).first()
        send_email('mail/payment_notify.html', user.email, 'Уведомление о зачислении платежа',
                   {'sum': form['withdraw_amount'], 'balance': user.balance})
    else:
        current_app.logger.info("User %s not found" % (form['label']))

    return ''


@bp.route('/robots.txt')
def robots():
    return Response("User-agent: *\r\nAllow: /\r\n", mimetype='text/plain')
