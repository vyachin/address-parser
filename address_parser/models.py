import datetime
import secrets

from werkzeug.security import generate_password_hash, check_password_hash

from . import db


class User(db.Model):
    STATUS_NEW = 0
    STATUS_CONFIRM = 1

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    api_key = db.Column(db.String(128), unique=True, index=True)
    register_at = db.Column(db.DateTime(), nullable=False, default=datetime.datetime.utcnow)
    login_at = db.Column(db.DateTime(), nullable=True)
    balance = db.Column(db.Float(), nullable=False)
    status = db.Column(db.Integer, nullable=False, server_default=str(STATUS_NEW))

    def __repr__(self):
        return '<User {}>'.format(self.email)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_api_key(self):
        self.api_key = secrets.token_urlsafe(32)


class Request(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)
    address = db.Column(db.String(256), nullable=False)
    result = db.Column(db.String(256))
    user = db.relationship("User", backref="requests")
    created_at = db.Column(db.DateTime(), nullable=False, default=datetime.datetime.utcnow)

    def __repr__(self):
        return '<Request {} {} {}>'.format(self.user_id, self.address, self.result)
