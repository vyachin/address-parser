import re

import nltk

COUNTRY = 'country'
COUNTRY_ID = 'country_id'
CITY = 'city'
CITY_ID = 'city_id'
REPUBLIC = 'republic'
STREET = 'street'
REGION = 'region'
REGION_ID = 'region_id'
HOUSE = 'house'
FLAT = 'flat'
DISTRICT = 'district'
BLOCK = 'block'
OFFICE = 'office'
LEVEL = 'level'
ROOM = 'room'
POSTCODE = 'postcode'
WORK_VILLAGE = 'village'
MAIL_BOX = 'mail_box'

split_pattern = re.compile(r'([а-яА-Я]+)(\d+)', re.VERBOSE)
house_block_pattern = re.compile(r'(\d+)К(\d+)', re.VERBOSE)


def address_tokenize(address):
    tokens = nltk.word_tokenize(address, language="russian")
    tokens = replace_tags(tokens)
    tokens = filter_repeat_tags(tokens)
    return tokens


def prepare_address(address):
    replaces = {'.': '. ', ':': ', ', ',': ', ', '№': ' ', ')': ' ', '(': ' ', '`': ' ', "'": ' ', '«': ' ', '»': ' ',
                '"': ' '}
    for i in replaces.items():
        address = address.replace(i[0], i[1])
    address = address.upper()
    address = address.replace('РОССИЙСКАЯ ФЕДЕРАЦИЯ', '')
    address = house_block_pattern.sub(r'ДОМ \1 КОРП \2', address)
    address = split_pattern.sub(r'\1. \2', address)
    return address


def address_parse(tagger, address):
    print("> %s" % address)

    address = prepare_address(address)
    print("= %s" % address)

    tokens = address_tokenize(address)
    tags = tagger.tag(tokens)
    print("# %s" % ' '.join(["%s/%s" % i for i in tags]))

    result = address_parts_from_tags(tags)
    print("< %s" % str(result))
    return result


def replace_tags(tags):
    result = []
    for t in tags:
        if t == 'РФ':
            continue
        if t == 'РБ':
            result.append('БАШКОРТОСТАН')
            t = 'РЕСП'
        if t == 'МО':
            result.append('МОСКОВСКАЯ')
            t = 'ОБЛ.'
        elif t == 'РА':
            result.append('АДЫГЕЯ')
            t = 'РЕСП'
        elif t == 'КК':
            result.append('КРАСНОДАРСКИЙ')
            t = 'КРАЙ'
        elif t == 'ЯНАО':
            result.append('ЯМАЛО-НЕНЕЦКИЙ')
            t = 'АО'
        elif t == 'РТ':
            result.append('ТАТАРСТАН')
            t = 'РЕСП'
        elif t == 'СПБ':
            result.append('САНКТ-ПЕТЕРБУРГ')
            t = 'Г.'
        result.append(t)

    return result


def filter_repeat_tags(tags):
    result = []
    prev = None

    for t in tags:
        if prev is None:
            prev = t
            result.append(t)
        elif t != prev:
            result.append(t)
            prev = t

    return result


def address_parts_from_tags(tags):
    result = dict()
    for (z, x) in tags:
        part = get_part_name(x)
        if part is None:
            continue
        if part in result:
            result[part] = result[part] + ' ' + z
        else:
            result[part] = z

    return result


def get_part_name(x):
    if x == 'CT':
        return COUNTRY
    if x == 'CN':  # x == 'C' or
        return CITY
    if x == 'RPN':  # x == 'RP' or
        return REPUBLIC
    if x == 'S' or x == 'SN':
        return STREET
    elif x == 'R' or x == 'RN':  #
        return REGION
    elif x == 'HN':  # x == 'H' or
        return HOUSE
    elif x == 'FN':  # x == 'F' or
        return FLAT
    elif x == 'D' or x == 'DN':
        return DISTRICT
    elif x == 'BN' or x == 'B':
        return BLOCK
    elif x == 'ON':
        return OFFICE
    elif x == 'LN':
        return LEVEL
    elif x == 'WVN' or x == 'WV':
        return WORK_VILLAGE
    elif x == 'RMN':
        return ROOM
    elif x == 'P':
        return POSTCODE
    elif x == 'MBN':
        return MAIL_BOX
    return None
