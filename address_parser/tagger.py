from pickle import load, dump

import nltk
from nltk.tag.brill import nltkdemo18plus

patterns = [
    (r'^[0-9]{6}$', 'P'),
    (r'.*', 'U'),
]

likely_tags = {
    'ОБЛ': 'R', 'ОБЛ.': 'R', 'ОБЛАСТЬ': 'R', 'КРАЙ': 'R',
    'Р-Н': 'D', 'РАЙОН': 'D', 'Р-ОН': 'D',
    'Д': 'H', 'Д.': 'H', 'ДОМ': 'H', 'ВЛД': 'H',
    'УЛ.': 'S', 'УЛ': 'S', 'УЛИЦА': 'S', 'ПЕР': 'S', 'ПЕР.': 'S', 'Ш.': 'S', 'ПР-КТ.': 'S', 'ПР-КТ': 'S', 'ПР-Д': 'S',
    'НАБ.': 'S', 'НАБ': 'S', 'Б-Р': 'S', 'ПРОСПЕКТ': 'S', 'МКР': 'S', 'Б-Р.': 'S', 'ШОССЕ': 'S', 'ПЕРЕУЛОК': 'S',
    'АЛЛЕЯ': 'S', 'ПЛ': 'S',
    'ПРОЕЗД': 'S', 'ТУП': 'S',
    'КВ.': 'F', 'КВ': 'F', 'КВАРТИРА': 'F',
    'ОФИС': 'O', 'ОФ': 'O',
    'ЭТАЖ': 'L',
    'ДЕРЕВНЯ': 'WV',
    'помещение': 'RM', 'пом.': 'RM', 'помещ.': 'RM', 'ПОМЕЩ': 'RM',
    '.': '.', ',': ',',
    'РЕСП': 'R', 'РЕСП.': 'R', 'РЕСПУБЛИКА': 'R',
    'Г.': 'C', 'Г': 'C', 'ГОР.': 'C', 'ГОР': 'C', 'ГОРОД': 'C',
    'ПГТ': 'WV', 'П.': 'WV', 'РП': 'WV', 'С.': 'WV', 'С': 'WV', 'ПОС.': 'WV', 'ГП': 'WV', 'СТ-ЦА': 'WV',
    'КОРП.': 'B', 'КОРПУС': 'B', 'СТРОЕНИЕ': 'B', 'ЛИТЕРА': 'B',
}


def get_tagged_sents(name):
    with open(name, 'r') as file:
        lines = file.read().split('\n')
        return [[nltk.tag.str2tuple(t) for t in i.split()] for i in lines if len(i) > 0]


def save_tagger(tagger, filename):
    with open(filename, 'wb') as file:
        dump(tagger, file, -1)


def load_tagger(filename):
    try:
        with open(filename, 'rb') as file:
            return load(file)
    except IOError:
        return None


def learn_tagger(trainer_set1, trainer_set2):
    regexp_tagger = nltk.RegexpTagger(patterns)
    tags = dict([(a.upper(), b) for (a, b) in likely_tags.items()])
    unigram_tagger = nltk.UnigramTagger(model=tags, backoff=regexp_tagger, verbose=True)
    unigram_tagger2 = nltk.UnigramTagger(trainer_set1, backoff=unigram_tagger, verbose=True)
    trainer = nltk.BrillTaggerTrainer(unigram_tagger2, nltkdemo18plus(), deterministic=True)
    tagger = trainer.train(trainer_set2, max_rules=20000, min_score=0)
    return tagger


def split_trainer_set(trainer_set):
    trainer_set_len = len(trainer_set)
    n1 = int(trainer_set_len * 0.3)
    n2 = int(trainer_set_len * 0.7)
    return trainer_set[:n1], trainer_set[n1 + 1: n2], trainer_set[n2 + 1:]
