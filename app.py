import os
from random import shuffle

import click
import nltk
from flask.helpers import get_env

from address_parser import create_app
from address_parser.parser import prepare_address, address_tokenize
from address_parser.tagger import get_tagged_sents, split_trainer_set, learn_tagger, save_tagger
from config import DevelopmentConfig, ProductionConfig

config_class = ProductionConfig
if get_env() == 'development':
    config_class = DevelopmentConfig

app = create_app(config_class)
basedir = os.path.dirname(os.path.abspath(__file__))


@app.cli.command("init")
def nltk_init():
    """Initialize NLTK"""
    nltk.download('punkt', download_dir=os.path.join(os.environ['VIRTUAL_ENV'], 'nltk_data'))


@app.cli.command("parse")
@click.argument("filename")
def parser_file(filename):
    with open(filename, 'r') as file:
        lines = file.read().split('\n')
        for address in lines:
            clean_address = prepare_address(address)
            tokens = address_tokenize(clean_address)
            tags = app.tagger.tag(tokens)
            print(address)
            print(' '.join(["%s/%s" % i for i in tags]), end="\n\n")


@app.cli.command("learn")
@click.argument("filename")
def nltk_learn(filename):
    """Learn NLP model"""
    tagged_sents = get_tagged_sents(filename)
    shuffle(tagged_sents)
    (a, b, c) = split_trainer_set(tagged_sents)
    tagger = learn_tagger(a, b)
    print(tagger.evaluate(c))
    save_tagger(tagger, app.config['TAGGER_FILE'])


if __name__ == '__main__':
    app.run()
