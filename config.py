import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    TAGGER_FILE = os.path.join(basedir, 'tagger.pkl')
    SECRET_KEY = 'sPsarFAVilQd+8DBw7sozQ=='
    SENTRY_DSN = "https://9f739b0944f5432bbf5ae921dcdf7b66@sentry.io/2612859"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    YANDEX_SECRET = 'Trb9h5TlJC0Yg+6BRYZJdZkb'
    RECEIVER = '410011032036126'
    SENDGRID_API_KEY = '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'
    FROM_EMAIL = 'support@address-parser.ru'
    REDIS_URL = "redis://localhost:6379/0"
    SESSION_TYPE = 'redis'
    SESSION_USE_SIGNER = False


class ProductionConfig(Config):
    USE_X_SENDFILE = False
    SQLALCHEMY_DATABASE_URI = 'mysql://address_parser:nRE2pfvZp7a8ywMn@localhost/address_parser'
    SESSION_COOKIE_SECURE = True
    DEBUG = False
    TESTING = False
    SERVER_NAME = 'address-parser.ru'
    PREFERRED_URL_SCHEME = 'https'


class DevelopmentConfig(Config):
    USE_X_SENDFILE = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.sqlite')
    DEBUG = True
    TESTING = False
